# <span style ="color : blue"> 2. Conception Assistée par Ordinateur (CAO) <span/>

Pour ce deuxième module, nous avons appris le principe des CAO. Plusieurs logiciels nous ont été introduits. 

## **<span style ="color : white">A. Qu'est-ce que c'est les CAO ?<span/>**

Les CAO sont les conceptions assistées par oridnateur (ou CAD en english). En gros, c'est l'ensemble de tous les logiciels qui vont nous permettre de développer une image et qui par la suite va pouvoir être développée en quelque chose d'utilisable. 

Durant ce module, on a appris l'utilisation de 3 d'entre eux. 


### **<span style ="color : grey">A.a Inkscape<span/>**

Un des premiers logiciels que nous avons découvert pour ce module était Inkascape. C'est un logiciel de dessin vectoriel, c'est-à-dire en 2D. 

J'ai téléchargé le logiciel via **[ce lien](https://inkscape.org/)**.

Un tutoriel d'utilisation est disponible juste [ici](https://inkscape.org/learn/tutorials/).

### **<span style ="color : grey">A.b OpenSCAD<span/>**

OpenSCAD est un logiciel qui permet la réalisation de forme 3D. Pour concevoir l'image 3D, il faut réaliser un code. C'est assez pratique car un code peut être partagé à d'autres personnes. Il existe donc pas mal de sites qui permettent directement de télécharger ces codes et de les utiliser pour son loisir personnel ! 

Piur apprendre les bases de l'utilisation, j'ai lu [la page d'introduction sur le logiciel](https://openscad.org/cheatsheet/). J'ai également regardé des vidéos d'explication sur Youtube. Ces tutos m'ont permise d'avoir en tête les bases d'utilisation du logiciel. 


POur commencer, j'ai lu [la page d'introduction sur le logiciel](https://openscad.org/cheatsheet/). J'ai également regardé des vidéos d'explication sur Youtube. Cela m'a permis d'avoir en tête les bases d'utilisation du logiciel. 


### **<span style ="color : grey">A.c FreeCAD<span/>**

C'est un logiciel qui permet de réaliser, grâce à des tracés de base (des lignes droites, des arcs de cercle...), des formes 2D. Ces formes 2D peuvent ensuite être exportées vers une forme 3D. 

Contrairement à OpenSCAD, il ne demande pas la réalisation d'un code. Il est peut-être donc plus facile à utiliser pour des gens n'ayant pas assez de bases en informatique. 


## **<span style ="color : white">B. L'objectif de cette semaine<span/>**

L'objectif de ce module était de concevoir un **compliant mechanism**.
 Selon Wikipedia (super source), un compliant mechanism est **_un méchanisme flexible qui réalise une force et un mouvement transmissinle à travers une déformation élastique_**. Plusieurs exemples de ce type peuvent être trouvés sur [le site de Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks). 


### **<span style="color : grey">B.a Mon compliant mechanism<span/>**

Au début j'avais envie de réaliser un complaint mecanism un peu élaboré. J'ai cependant vite remarqué que mes quelques bases en informatique ne me le permettaient pas (snif). Je me suis donc rabattue sur une design "un peu plus simple", mais au moins j'ai réussi à obtenir quelques choses.  

Je suis donc partie sur celui-ci : 


<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q90/922/3XN470.png" alt="rp">
</p>



### **<span style ="color : grey">B.b Le code final<span/>**



<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q90/922/syGJ2z.png" alt="rp">
</p>


## **<span style ="color : white">B.b Les différentes License creative commons <span/>**

Lors de cette séance nous avons appris dans un deuxième temps le différents types de license qui existent. 

Qu'est-ce que c'est ? 

En gros ce sont des licenses publiques, qui permettent d'indiquer aux autres personnes ce qu'elles sont permises de faire avec un travail que l'on a réalisé. 

Il en existe plusieurs : 

- CC BY : 
-  CC BY-SA : 
- CC BY-NC
-  CC BY-ND
- CC BY-NC-SA
-  CC BY-NC-ND

Pour en apprendre plus sur ces licenses, [ce site](https://creativecommons.org/about/cclicenses/) est très bien fait et explique très clairement l'utilisation de chacun d'entre elles. 
