
# <span style ="color : blue">1. La documentation</span>

Ce premier module consistait à apprendre comment réaliser une bonne documentation. Il est très important lors de la réalisation d'un projet de faire une bonne documentation. Elle permet en effet de garder des traces du travail réalisé jusqu'à l'objectif fixé ! Il est alors très important de bien comprendre comment tous les outils pour la réaliser fonctionnent. 



## **<span style ="color: white"> A. Les outils utiles</span>**

Plusieurs outils sont utilisés pour réaliser la documentation.

### **<span style ="color: grey"> A.a Command Line user Interface aka CLI </span>**

Un CLI est le moyen de "communication" entre nous et l'ordinateur. Il nous permet d'exécuter des modifications sur notre site. 

Je n'ai pas dû télécharger de CLI car en ayant un mac, j'ai un terminal est directement intégré dans les applications. 


### **<span style ="color: grey"> A.b Gitlab</span>**

GitLab est un site qui permet de travailler facilement sur un projet. 


Pour pouvoir l'utiliser et mofifier nos infos, il faut créer une clef. Cette clef est un _SSH key_. Les étapes de son installation sont disponibles [ici](https://docs.gitlab.com/ee/user/ssh.html).



## **<span style ="color: white"> B. Editer le site internet</span>**

Sur GitLab, chaque étudiant a et doit réaliser son "site internet". Cela permet d'avoir chacun une petite introduction sur soi-même et surtout la documentation des projets que nous allons réaliser. 

Pour réaliser ce site, nous utilisons  _Visual studio_.

### **<span style ="color: grey">B.a Visual code</span>**


Avant toute chose, il faut télécharger _Visual studio_. Il permet de modifier le site à partir d'un terminal. Je l'ai téléchargé via ce [lien.](https://code.visualstudio.com)

Ensuite, afin de comprendre l'utilisation de cette application et d'apprendre quelques commandes de base, j'ai regardé [cette vidéo Youtube](https://www.youtube.com/watch?v=34_dRW42kYI). Je la recommande pour les débutants comme moi ! 


| Commande |  Rôle   |  Exemple |          
|-----|-----------------|---------|
| `**phrase**` |  écriture en gras | **ma pizza préférée est celle aux 4 fromages** |  
| `_phrase_`  | écriture en italique | _ma pizza préférée est celle aux 4 fromages_|  
| `<span style ="color:choixcouleur"> </span>`| écriture en couleur | <span style ="color:red"> ma pizza préférée est celle aux 4 fromages</span>  | 
| `[lien](liendusite)` | lien vers un site | [lien](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/)|   
| `![image](lienimage)` | affichage image |  |   |        |


### **<span style ="color: grey">B.b Finitions</span>**

Une fois satisfait.e de la mofification de notre site , il faut "l'envoyer" grâce à quelques commandes sur le terminal. 

La première étape est d'introduire `git status`. Cette commande permet de visualiser les modifications réalisées dans un document.

Deuxièmement, il faut introduire `git add -A`. 

Ensuite, il faut mettre `git commit -m"introduction du message que l'on veut"`

Finalement, il faut mettre `git push`, c'est l'étape finale qui envoie toutes les modifications directement sur le site internet de Git Lab. On peut suivre tous les push réalisés directement sur notre [profil.](https://gitlab.com/lucielesti)

Le site internet est mis à jour seulement deux fois par semaine : le mardi et le jeudi. 



![code](https://imagizer.imageshack.com/v2/640x480q90/922/zbRoOj.png)



