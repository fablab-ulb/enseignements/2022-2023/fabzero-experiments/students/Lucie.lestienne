#  <span style ="color : blue"> 4. Laser cutter <span/>

Pour cette semaine, nous avons du choisir une formation entre plusieurs sur des outlis du Fablab. Il y avait les microcontrolleur, les CNC, les 3D repair et finalement les laser cutter. 
J'ai décidé de partir sur la formation laser cutter. J'ai surtout fait en fonction des personnes avec qui je travaillais pour qu'on soit formés sur un maximum de machines. 
Avant de commencer la séance, nous avons eu un petit quizz pour voir si nous avions bien lu le manuel d'instructions.

## **<span style ="color : white">A.  Les machines<span/>**

Au FabLab, il y a 3 machines laser. Il y a **Epilog Fusion Pro 32**, **Lasersaur** et **Full Spectrum Muse**.

### **<span style ="color : grey">A.a Epilog Fusion Pro 32**<span/>

Il s'agit d'une des deux grandes machines laser du FabLab. Elle dispose d'une assez grande surface de découpe, ce qui est pratique pour les grands design. Son utilisation est assez simple. Il est d'ailleurs conseillé d'utiliser cette machine plutôt que les autres si toutes les machines laser sont disponibles. 

Le manuel d'utilisation de cette imprimante se trouve [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Epilog.md).


### **<span style ="color : grey">A.b Lasersaur**<span/>

Cette machine a été développée par le FabLab. Elle est donc un peu différente au niveau de son utilisation, mais fonctionne tout aussi bien que les deux autres. Pour celle-ci, il ne faut **SURTOUT** pas oublier d'allumer les 3 machines suivantes qui y sont reliées (on les retrouve derrière) : l'extracteur de fumée, la vanne d'air comprimé et le refroidisseur à eau. Si l'allumage de l'une d'entre elles a été oublié, un petit bouton en bas à gauche de l'ordinateur (bouton **Status**) apparaitra rouge. Il ne sera vert quand toutes les machines seront bien allumées, et donc le laser prêt à être utilisé. 

Elle a la surface d'impression la plus grande de toutes les machines. 

Sur cette machine, pour sélectionner quelles parties nous souhaitons graver et celles que nous voulons couper, il suffit de sélectionner les couleurs que l'ordinateur reconnait sur notre dessin 2D. Les parties à graver sont les premières que l'on doit sélectionner, et ensuite passent celles à couper. On peut ensuite régler la vitesse et la puissance : 


<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q70/923/84b58R.jpg" alt="rp">
</p>


Son manuel d'utilisation est disponible [ici](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/Lasersaur.md).

### **<span style ="color : grey">4.1.3. Full Spectrum Muse**<span/>

Cette machine est la plus petite. Cependant, elle est très pratique à utiliser car on peut directement brancher son ordinateur en se connectant à son wifi. 

Voici quelques vidéos Youtube qui expliquent son utilisation [manuel d'utilisation](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6). 


## **<span style ="color : white">B. Le design**<span/>

Le design que le laser créera est en 2D. Il peut être réalisé sur des logiciels comme InkScape, dont nous avons appris l'utilisation lors du premier module. 
Il faut absolument que le design soit sous forme vectorielle sous peine que l'ordinateur lié au laser ne sache pas réaliser la forme souhaitée. 
Il faut ensuite exporter notre design sous format .svg, puis le mettre sur une clef USB afin de l'introduire dans la machine laser.

## **<span style ="color : white">C. Quelques précautions avant l'utilisation...**<span/>

Quelques règles sont à respecter lorsque l'on souhaite utiliser une des machines laser. 

- **Ne jamais quitter la pièce lorsqu'une pièce est en cours de fabrication. Si on a une urgence (genre aller faire pipi), il y a un bouton pause.**
- **Porter des lunettes spéciales (quelques paires sont disponibles dans la salle laser) est fortement recommandé lorsque l'on veut regarder dans la machine. Le laser peut abimer l'iris.**
- **Ne jamais ouvir la machine en cours d'utilisation et lorsque c'est fini mais qu'il reste de la fumée à l'intérieur**
- **Toujours allumer l'extracteur de fumée et l'air comprimé**


## **<span style ="color : white">D. Quelques réglages et c'est parti !**<span/>

Sur les machines, il n'y a que deux paramètres à modifier avant de lancer le découpage : ce sont la **puissance** et la **vitesse**. 
Plusieurs petits "panneaux" sont disponibles dans la salle avec les différents matériaux. Il s'y trouve un tableau à double entrée avec différentes vitesses et puissances. 
De manière générale, on utilisera une grande vitesse avec une faible puissance pour les matériaux fins, et une grande puissance et une faible vitesse pour les métariaux plus épais. 


## **<span style ="color : white">E. Ce que j'ai réalisé pendant la séance**<span/>


Après un petit tour dans la salle des laser et une bonne explication sur leur fonctionnement, nous avons dû travailler en  petit groupe afin de tester les machines et développer notre premier petit objet avec un des laser. J'ai travaillé avec [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/fabzero-modules/module04/). 
Nous avons décidé de partir sur un petit porte épices, parce que **[Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/fabzero-modules/module04/)** en avait besoin d'un et avait envie d'offri un cadeaux à ses collocs (elle est trop sympa). 

Pour le design principal nous avons en avons pris un déjà tout fait sur le site [box.py](https://www.festi.info/boxes.py/). Nous n'avions plus beaucoup de temps mais voulions vraiment développer quelque chose de sympa, donc nous avons choisi la facilité pour cette fois-ci. 
Nous avons quand même modifié les rayons des trous pré-existants afin que les pots à épices puissent y entrer. Nous avons également écrit "Hé pisse" (petit jeu de mot) sur le design, car le but de cette séance était de développer un objet coupé mais également gravé. Pour ce faire nous avons ouvert le document télécharge sur le site [box.py](https://www.festi.info/boxes.py/) sur le logiciel InkScape (voir utilisation au premier module). Après modification, nous avons bien fait attention de sauvegarder le nouveau design sous forme vectorielle. 

Voici ce que cela donne sur ordinateur...

<p align="center">
  <img src=" https://imagizer.imageshack.com/v2/640x480q70/922/Bvr3KH.jpg" alt="rp">
</p>


... et ce que ça donne une fois terminé ! 


<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q70/924/pH2BsE.jpg" alt="rp">
</p>


**EN BONUS**

<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q70/923/05dAmL.jpg" alt="rp">
</p>


Toujours bien porter ses lunettes !!!!! 