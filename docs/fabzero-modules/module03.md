# <span style ="color : blue" > 1. Impression 3D </span>

Pour ce module, nous avons appris à utiliser une imprimante 3D. C'était une formation obligatoire et importante car celle-ci nous permet de devenir autonome au niveau de l'utilisation de ces imprimantes. 

## **<span style ="color : white" > A. PruscaSlicer </span>**

La première étape était d'installer le logiciel [PruscaSlicer](https://www.prusa3d.com/fr/page/prusaslicer_424/). C'est un logiciel de tranchage qui permet d'imprimer la forme développée sur les logiciels vus au module 2 ( OpenScad, FreeScad,). C'est un logiciel facile à utiliser, il est largement connu dans le monde des imprimantes 3D et c'est surtout un logiciel **gratuit**.

### **<span style ="color : grey" > A.a Utilisation </span>**

Nous avons ensuite suivi le [tutoriel d'utilisation](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) du logiciel PruscaSlicer. 


- La première étape est d'importer le fichier comportant le code de notre pièce dans le logiciel. Cette étape est assez simple car il suffit de cliquer sur le petit carré en haut à droit et de choisir le fichier en question.


<p align="center">
  <img src="https://imagizer.imageshack.com/v2/320x240q90/924/I9V9em.png" alt="rp">
</p>


- Il faut ensuite choisir le bon modèle d'imprimante. C'est important car si ce n'est pas fait la pièce risque de mal s'imprimer. Au FabLab, il s'agit du modèle [Original Prusa i3 MK3S+](https://www.prusa3d.com/category/original-prusa-i3-mk3s/). Pour sélectionner une imprimante, il suffit de se rendre en haut à droite => dérouler la barre "imprimante => sélectionner le bon modèle. 

- Certains paramètres de l'impression sont pré-définis par question de facilité : la hauteur des couches est fixée à **0,2 mm** et le taux de remplissage est généralement établi à **15%**.

- Pour finir, il faut exporter le G-code sur carte SD pour ensuite l'introduire dans l'imprimante. 




## **<span style ="color : white" > B. L'impression de ma pièce </span>**

Le but premier de ce module était d'impirmer la pièce que nous avions développée suite au Module 2. 

J'avais décidé de partir sur cette pièce : 

<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q90/922/3XN470.png" alt="rp">
</p>


Pour imprimer la pièce : 

- D'abord vérifier que le plateau est propre. S'il ne l'est pas, le frotter pour enlever les impuretés.

- Ensuite, il faut insérer la carte SD et sélectionner grâce à la petite roulette le fichier que l'on souhaite imprimer.

- Une fois le fichier sélectionné, il faut un peu de temps pour que l'imprimant chauffe, fasse ses réglages et commence l'impression ! 

- On peut vérifier directement si l'impression va bien se faire : il s'agit de l'impression de la jupe.  Elle permet de vérifier l'adhérence de la première couche sur le plateau d'impression. Si elle se détache, c'est signe que l'impression va mal se réaliser. 

- Il arrive que l'impression ne se fasse pas à cause du fil de la bobine. Il existe des paramètres dans l'imprimante qui permettent d'expulser le fil, le remettre correctement et ensuite le réintroduire dans l'imprimante pour que l'impression puisse se faire correctement. 


De mon côté, l'impression de ma pièce ne s'est pas bien passée. Je n'avais malheureusement pas bien défini certains modes d'impression sur le logiciel (on apprend de ses erreurs comme on dit). Il n'y a donc seulement qu'une petite partie de ma pièce, la couche inférieure qui s'est imprimée. De plus, elle s'est directement cassée lorsque j'ai essayé de l'enlever de la machine... 

Voici quand même une petite photo de l'impression de ma "pièce". 


<p align="center">
  <img src="https://imagizer.imageshack.com/v2/640x480q70/922/aOneuk.jpg" alt="rp">
</p>




## **<span style ="color : white" > C. Assemblage avec d'autres pièces </span>**


Un autre assignements pour aujourd'hui était l'élaboration d'un kit, c'est-à-dire assembler sa pièce avec une ou des pièces réalisées par d'autres étudiants. 

Etant donné que la mienne ne s'est pas imprimée je n'ai pas vraiment pu réaliser cette partie de l'assignement. J'ai néanmoins pu observer les kits réalisés par les autres étudiants. 
