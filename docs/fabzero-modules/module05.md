#  <span style ="color : blue"> 5. Dynamique de groupe et projet final <span/>5. 
 
 Ce module regroupe l'ensemble des documentations sur les séances et les cours que nous avons suivis concernant la dynamique de groupe, la gestion et finalement la formation de notre projet final ! 

Dans un premier temps, il s'y trouve l'apprentissage des arbres à problèmes et solutions. 
Dans un deuxième temps, il y a la formation des groupes et l'apprentissage de la gestion de ce groupe pour la réalisation du projet final ! 


##  <span style ="color : white"> 5.1. Exercice de résolution face à une problématique <span/>

Lors de notre projet final, et plus généralement au cours de notre vie, nous seront confrontés à différentes problématiques qu'il nous faudra apprendre à gérer et par la suite à résoudre. Une manière de faire face à ces problématiques se trouve dans les concepts d''arbre à problème et d'arbre à solution. Ces concepts sont faciles à prendre en main et donnent des résultats encourageants. Une courte vidéo explicative est disponible ici.

Pour la réalisation de la documentation, j'ai travaillé avec mes collègues [Emma Dubois](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/), qui s'est concentrée sur la mise en forme des idées et [Martin Gilles](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/) qui a réalisé toute la documentation, ceci explique pourquoi vous retrouverez la même documentation sur sa page et la page d'Emma. Merci à lui pour l'avoir fait !


###  <span style ="color : grey"> 5.1.1 Identifier le problème principal = tronc <span/>

Après avoir parcouru les 17 Sustainable Development Goals listés par les nations unies, nous avons décidé de nous intéresser à la problématique de l'accès à l'eau potable en Afrique. En effet, dans le cadre du cours de pollution du mileu physique, une slide nous avait particulièrement marqué:

 <p align="center">
  <img src="https://user-content.gitlab-static.net/70e05d9aa5109986d6720f9c7180231cf42d856e/68747470733a2f2f696d6167697a65722e696d616765736861636b2e636f6d2f76322f363030783430307137302f3932332f47536b7053752e706e67 " alt="rp">
</p>




Cette slide démontre les problèmes liés à l'accès à l'eau potable dans certaines parties du globe. Nous pouvons observer qu'en Europe, malgré une disponiblité en eau plus faible proportionnellement à l'Afrique, la population ne souffre en général pas ou peu du manque d'eau. Un problème qui est au contraire très présent en Afrique et nous allons essayer de comprendre pourquoi.
Afin de rendre notre problèmatique plus concrète et nos actions plus réalisables, nous avons essayé de cibler un problème précis et nous nous sommes renseigné sur l'accès à l'eau potable au Sénégal.

L’accès à l’eau potable demeure un problème au Sénégal, pour certaines populations, en particulier celles des quartiers pauvres ainsi que des zones rurales. De nombreuses localités ne disposent pas encore d’eau courante à domicile et s’approvisionnent à partir des puits et bornes fontaines publiques. Jusqu’à fin 2009, un peu plus de 26% de la population rurale s’approvisionnaient encore à partir de sources d’eau non « potabilisée ». En 2015, ce taux se réduit à 14%.



Source : CNCD



###  <span style ="color : grey"> 5.1.2. Identifier les causes = racines  <span/>
Après une recherche biblographique sur le sujet, nous avons identifié différentes causes ayant un impact plus ou moins équivalent.


- Privatisation de l'eau potable sous forme d'un partenariat publique-privé

- Manque d'investissement dans les infrastructures

- Croissance démographique exponentielle


- Réchauffement climatique, impliquant aridité, érosion et ruissellement.


###  <span style ="color : grey"> 5.1.3. Identifier les conséquences = branches <span/>

Les principales conséquences qui en découlent sont les suivantes :


- Maladie hydrique et de santé publique

- Instabilité croissante dans le pays entre les autorités et la population locale

- Augmentation du cout de l'eau

- Problèmes d'hygènes (de nombreux habitants retournent à des moyens basiques d'extractions d'eau comme les puits ou l'irrigation)


###  <span style ="color : grey"> 5.1.4. Notre arbres à problèmes<span/>


<p align="center">
  <img src="https://user-content.gitlab-static.net/16cf84d43e013dc42e7e19fc2c0c7ef3a2e0acb4/68747470733a2f2f696d6167697a65722e696d616765736861636b2e636f6d2f76322f383030783630307137302f3932322f3276575873722e706e67" alt="rp">
</p>

###  <span style ="color : grey"> 5.1.5. Arbre à objectifs <span/>

Avec l'arbre à objectif, l'idée est de tranformer le problème identifié en un objectif à atteindre pour le solutionner, tout en se donnant les moyens de remplir cet objectif.

###  <span style ="color : grey"> 5.1.6. Problèmes -> objetcifs  <span/>
Dans notre cas, nous avons considéré qu'une amélioration des infrastructures d'accès à l'eau était un objectif concret et réalisable qui pourrait offrir une solution à notre problématique.

###  <span style ="color : grey"> 5.1.7. Causes -> Activités <span/>

A. Sensibilisation globale

L'idée serait de conscientiser le monde (et principalement les jeunes) à notre problématique afin d'élargir le spectre d'entités capables de trouver des solutions. Pour cela, nous pourrons faire appel par exemple à EYP (European Youth Parliament). Une organisation dont le but est de faire participer des jeunes venus de toute l'Europe à des séminaires de réfléxions sur certaines thématiques et de trouver des solutions pratiques à celles-ci.

B. Sensibilsation locale

A travers Amnesty International, il est possible de lancer des actions de plaidoyer visant les chefs d'état d'un certain pays pour cibler une problématique spécifique. Le but serait alors de déprivatiser l'accès à l'eau afin de transiter vers une politique publique de celle-ci.

C. Soutien financier

Des récoltes de fonds organisés par l'ONG Eau-vives pourraient être relayées à plus grandes échelle afin de créer un budget conséquent capable de soutenir la construction de nouvelles infrastructures.

D. solutions pratiques

Une façon de trouver des solutions pratique serait de faire appel au réseau des différents Fablab. Comme lors des inondations en Inde, il serait alors possible de mobiliser plusieurs Fablab dans le monde afin de créer et d'établir des solutions rapides et facilement applicables par les locaux.

###  <span style ="color : grey"> 5.1.8. Conséquences -> résultats  <span/>


Déprivatisation de l'eau potable
Amélioration de l'hygène de vie des habitants
Augmentation des offres d'emplois dans le pays cible
Diminution du coût de l'eau


###  <span style ="color : grey"> 5.1.9. Notre arbre à solutions <span/>




<p align="center">
  <img src="https://user-content.gitlab-static.net/603331ff8c432cd9e1394a41ce21bd2874fd26f3/68747470733a2f2f696d6167697a65722e696d616765736861636b2e636f6d2f76322f383030783630307137302f3932332f564a577833622e706e67" alt="rp">
</p>


##  <span style ="color : blue"> 5.2. Formation des groupes et début du brainstorming <span/>

Cette séance a été très importante car elle a permis la formation des groupes finaux pour le projet final. 

Je n'étais malheureusement pas su être en présentiel ce jour-là, mais je me suis débrouillée à distance pour pouvoir réaliser les assignements. 

### <span style ="color : white"> 5.2.1 Formation des groupes <span/>

Malgré le fait que je n'ai pas été présente sur place, j'ai demandé à mon ami [Martin Gilles](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/-/blob/main/docs/fabzero-modules/module05.md) d'apporter l'objet que nous devions présenter. Cet objet devait représenter une problématique "qui nous tient à coeur". Pour ma part, j'ai choisi de présenter un gobelet de café jetable, que l'on retrouve dans tous les cafés qui vendent des cafés take-away, et que l'on retrouve également à l'université dans les différents restaurants universitaires. 

<p align="center">
  <img src="https://d11o8pt3cttu38.cloudfront.net/wp-content/uploads/2018/02/.cleancup-zéro-déchet-zéro-gobelet.jpg" alt="rp">
</p>



Pourquoi une cup de café jetable ? 

Pour moi, cet objet représentait la surutilisation des objets à usages uniques, de manière générale mais plus précisément à l'université comme dans les restaurants ou pour les cours. 

En effet, il est possible à l'université de prendre ses plats à emporter. Ces plats nous sont alors servis dans des boites en carton, avec un petit set contenant fourchette, couteau, cuillère et serviette, jetables également. En plus des plats, il est possible de prendre des cafés à emporter : les gobelets sont également jetables. 

Malgré une initiative de l'université, proposant désormais des cup de café réutilsables, qui permet en plus d'avoir certaines réductions pour les boissons, le problème de la surutilisation de ces emballages n'est absolument pas réglé. Chaque jour, des kilos d'emballage sont jetés sur le campus. 

Suite à la présentation de son objet par chaque étudiant, des groupes se sont formés en fonction des ressemblances entre les problématiques. On m'a donc placée dans le grouep avec : 

- [Jules]()
- [Gilles]()
- [Jonathan]()
- [Thomas]() 

Les objets qu'ils avaient apportés (en général des gourdes) se rapprochaient pas mal de mon idée. Notre idée générale est la surutilisation des objets à usage unique. 

### <span style ="color : grey"> 5.2.3 Brainstorming <span/>

Je n'étais pas présente lors de cette partie de la séance non plus, mais elle consistait globalement à discuter de problématiques avec son groupe nouvellement formé, d'y associer des mots et de par la suite en discuter avec les autres groupes. 


## <span style ="color : white"> 5.3 Outils de la dynamique de groupe <span/>

La gestion d'un group est un point très important lors de la réalisation d'un projet. Elle permet en effet la bonne entente et 

Plusieurs outils nous ont été présenté, j'ai décidé d'en présenter trois d'entre eux : 

### <span style ="color : grey"> 5.3.1 Le déroulement de la réunion <span/>

C'est selon moi l'un des points les plus importants dans un projet. Il permet en effet à ce que les réunions, et le projet en général se déroulent de façon claire, rapide et surtout efficace. 

Le plan de déroulement de la réunion peut se faire soit avant, soit au tout début de celle-ci. Faire le plan de la réunion avant me paraît être la manière la plus efficace car elle permet à tous les membres du groupe d'avoir les points qui seront abordés en tête et de potentiellement rajouter des points avant réunion. 

### <span style ="color : grey"> 5.3.2. La répartition des différents rôles <span/>

Plusieurs rôles doivent être répartis entre les membres du groupe. Il est important qu'il y ait une certaine rotation entre chaque membre pour que les rôles ne soient pas donner chaque fois à la même personne ! 

- Le ou la  secrétaire : son rôle est fondamental car il/elle permet de garder des traces écrites de la réunion.

- L'animateur : son rôle est de relancer des sujets s'il voit que la conversation tourne en rond et n'aboutit à rien. 

- celui qui gère le temps : il est le maître du temps de la réunion ! Il permet par exemple de gérer le temps de parole de chacun.

### <span style ="color : grey"> 5.3.3. La météo d'entrée <span/>

Ce petit outil permet à chaque membre du projet de partager aux autres la manière dont il/elle se sent aujourd'hui, son  mood du moment. Cela permet à tout le monde de savoir et de comprendre le comportement des membres et ainsi d'éviter les tensions lors du projet ! 

C'est une étape plus rapide et simple lors d'une réunion mais je trouve qu'elle permet de commencer la réunion sur un bon pied. 
