
# **Salut !**

Bienvenu.e sur mon site, élaboré dans le cadre du cours ["How to make (almost Any Eperiments using digital Fabrication" de l'année académique 2022-2023 ! ](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/) Vous y retrouverez une petite présentation personnelle, et surtout l'évolution de mon projet pour ce cours. 

Bonne lecture ! 



## **<span style ="color : pink">Que savoir de moi ? </span>**

Moi, c'est <span style ="color : purple"> Lucie</span> ! J'ai 20 ans et je suis actuellement en 3 ème année de bachelier en bioingénieur. 
Je suis née à Bruxelles, j'y ai habité 5 ans ( Saint-Gilles vie) mais j'ai ensuite déménagé et grandi dans la petite ville de Tournai, dans la province du Hainaut. C'est très petit, mais n'hésitez pas à venir visiter sa belle cathédrale et son beffroi quand meme classés Patrimoine Mondial de l'UNESCO (la classe)... 
Je suis revenue à Bruxelles pour y faire mes études il y a déjà presque trois ans ! 

![moi](https://imagizer.imageshack.com/v2/640x480q90/923/SHTze6.jpg)

## **<span style ="color : pink"> Qu'est-ce que j'étudie ? </span>**

Je suis actuellement en troisième année de bachelier en bioingénierie à l'EBB aka l'école de bioingénierie de Bruxelles. En rentrant à l'université, j'avais peur de ne pasaimer ces études. Mais après (déjà) trois ans en bioingé, je ne regrette pas du tout mon choix ! Ce sont des études très diversifiées, qui touchent à plein de suets mais surtout très d'actualité en vue des problèmes environnemtaux que nous rencontrons. J'ai hâte de pouvoir être sur le terrain et appliquer mes connaissances ! 
Pour l'année prochaine, je n'ai asbsolument **aucune** idée de ce que je voudrais faire. J'ai malheureusement raté toutes les deadlines d'inscrption pour les Erasmsus et les Master à l'étranger.  

Pour notre dernière année de bachelier, nous devions choisir un projet. J'ai choisi le projet radeau végétal ! ( Tout comme mon collègue [Martin Gilles](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin/)), qui est d'ailleurs mon binôme de soutien pour ce cours. Il m'aide beaucoup et je le remercie ! Si j'étais prof, je lui aurais mis un point bonus...)

Pour revenir au projet, le but de notre radeau est, et cela grâce aux plantes que nous avons fixées dessus, de parvenir à filtrer et dépolluer les eaux de certains nutriments qui y sont en excès (comme le phosphore ou l'azote). C'est un très chouette projet qui nous permet de travailler avcec Bruxelles Environnement mais surtout de mettre un pied réellement sur le terrain, et de découvrir la joie du travail en groupe... 


 ![radeau](https://imagizer.imageshack.com/v2/640x480q90/922/FMwvgl.jpg)

## **<span style ="color : pink"> Mes objectifs pour ce cours ?</span>**

Il faut savoir que l'informatique et moi, ca fait 2. Je ne m'imaginais pas qu'il y aurait autant d'informatique et de travail sur l'ordinateur, mais c'est une bonne occasion de s'améliorer et d'acquérir de nouvelles compétences dans le domaine de l'informatique ! 
J'ai hâte de voir sur quel projet on va partir ! 